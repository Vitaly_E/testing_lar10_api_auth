<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['name','is_completed'];//fields can be updated via PUT. is_completed can be updated via PATCH and CompleteTaskController
}
