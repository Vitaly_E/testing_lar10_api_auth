<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

//class UpdateTaskRequest extends FormRequest --> wrong
class UpdateTaskRequest extends StoreTaskRequest
{

    public function authorize()//: bool
    {
        //die($this->user());

        $user = $this->user();

        return $user != null and $user->tokenCan('update');

        //return true;
    }

}
