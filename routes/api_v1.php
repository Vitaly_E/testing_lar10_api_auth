<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\CompleteTaskController;

Route::group(['middleware'=>'auth:sanctum'], function (){
    Route::get('/hi', function (Request $request) {
        return 'hi';//$request->user();
    });

    Route::apiResource('tasks', \App\Http\Controllers\Api\V1\TaskController::class);
    Route::patch('tasks/{task}/complete', CompleteTaskController::class);

});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
